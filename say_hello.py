#!/usr/bin/env python3.7

import netmiko
import time

# This file is used for test exit_code when an Ansible script run a Python script
def sayHello():
    
    print("[sayHello] Hello World")
    
    # Always Success
    exit(0)


if __name__ == "__main__":
    sayHello()
