# How to use Python "exit_code" with Ansible

It's very easy. 

* If your Python script ends with ```exit(0)``` => SUCCESS
* If you Python script ends with ```exit(1)``` => FAILURE



```bash
(master)⚡ % ansible-playbook test_exit_code.yml                                                                    /Volumes/Data/prog/ansible-python-exit_code
 [WARNING]: Unable to parse /etc/ansible/hosts as an inventory source

 [WARNING]: No inventory was parsed, only implicit localhost is available

 [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

 __________________
< PLAY [localhost] >
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

 ____________________________________
< TASK [Run Python Random Exit Code] >
 ------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

changed: [localhost]
 ______________________
< TASK [Run SAY HELLO] >
 ----------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

changed: [localhost]
 ____________
< PLAY RECAP >
 ------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

localhost                  : ok=2    changed=2    unreachable=0    failed=0

(master)⚡ % ansible-playbook test_exit_code.yml                                                                    /Volumes/Data/prog/ansible-python-exit_code
 [WARNING]: Unable to parse /etc/ansible/hosts as an inventory source

 [WARNING]: No inventory was parsed, only implicit localhost is available

 [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

 __________________
< PLAY [localhost] >
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

 ____________________________________
< TASK [Run Python Random Exit Code] >
 ------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

fatal: [localhost]: FAILED! => {"changed": true, "msg": "non-zero return code", "rc": 1, "stderr": "", "stderr_lines": [], "stdout": "[generateRandomExitCode] clock - 1548939040621\n[generateRandomExitCode] exit_code - 1\n", "stdout_lines": ["[generateRandomExitCode] clock - 1548939040621", "[generateRandomExitCode] exit_code - 1"]}
        to retry, use: --limit @/Volumes/Data/prog/ansible-python-exit_code/test_exit_code.retry
 ____________
< PLAY RECAP >
 ------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

localhost                  : ok=0    changed=0    unreachable=0    failed=1

(master)⚡ [2] %   
```

If value is odd => Failed (1548939040621)