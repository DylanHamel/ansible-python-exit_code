#!/usr/bin/env python3.7

import netmiko
import time

# This file is used for test exit_code when an Ansible script run a Python script
def generateRandomExitCode():
    
    # exitCode()

    clock = int(round(time.time() * 1000))
    print("[generateRandomExitCode] clock -", clock)
    
    exit_code = (int(clock) % 2)
    print("[generateRandomExitCode] exit_code -", exit_code)

    exit(exit_code)

def exitCode():
    exit(1)


if __name__ == "__main__":
    generateRandomExitCode()
